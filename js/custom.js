// Header // 
jQuery(document).ready(function ($) {
    jQuery('header a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery("nav li a").removeClass("active");
                jQuery(this).addClass('active');
                jQuery('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

// Sticky //

jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() > 1) {
        jQuery('header').addClass("sticky");
    } else {
        jQuery('header').removeClass("sticky");
    }
});

// Textarea First Letter Capital //
jQuery('textarea.form_control1,textarea.form_control2,textarea.form_control3').on('keypress', function (event) {

    var $this = jQuery(this),
        thisVal = $this.val(),
        FLC = thisVal.slice(0, 1).toUpperCase();
    con = thisVal.slice(1, thisVal.length);
    jQuery(this).val(FLC + con);
});
